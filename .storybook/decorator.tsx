import React from "react";
import { ThreeDCanvas } from "../src/stories/helpers/3DCanvas";

export const Decorator = (Story, options) => {
  const { parameters } = options;
  return (
    <ThreeDCanvas
      debug={parameters.debug}
      camera={parameters.camera}
      controls={parameters.controls}
      wrapperStyle={{
        minHeight: "calc(100vh - 2rem)",
        height: "calc(100vh - 2rem)",
        width: "calc(100vw - 2rem)",
        backgroundColor: parameters.backgroundColor,
      }}
      addLight={parameters.addLight}
    >
      <Story />
    </ThreeDCanvas>
  );
};
