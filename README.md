# Random Distributor

Randomly distribute points within shapes (Rectangle, Cube, Sphere, Circle).

Uniform : [a type of probability distribution in which all outcomes are equally likely](https://www.investopedia.com/terms/u/uniform-distribution.asp#:~:text=In%20statistics%2C%20uniform%20distribution%20refers,a%20spade%20is%20equally%20likely.)

Center : positions will be distributed around the center of the shape. [Normal distribution, also known as the Gaussian distribution, is a probability distribution that is symmetric about the mean, showing that data near the mean are more frequent in occurrence than data far from the mean.](https://www.investopedia.com/terms/n/normaldistribution.asp)
You can play with mu (mean) and sigma (standard deviation) to sculpt the bell curve shape.

Corners / Edges : positions will be close to the shape borders. These are tweaked "centered" distribution.

The math are from [D3.random](https://github.com/d3/d3-random)

Implemented with typescript and esm.

```javascript
import { distributor, type Position } from "randomDistributor"

let positions: Position[];

positions = distributor.getRectangle({w: 1920, h: 1080}).uniform(1000)
positions = distributor.getRectangle({w: 1920, h: 1080}).center(1000, { mu: 0.5, sigma: 0.1 })
positions = distributor.getRectangle({w: 1920, h: 1080}).corners(1000, { sigma: 0.13 })

//getCube(int halfWidth)
positions = distributor.getCube(250).uniform(1000)
positions = distributor.getCube(250).corners(1000, { sigma: 0.13 })
positions = distributor.getCube(250).center(1000, { mu: 0.5, sigma: 0.1 })

//getSphere(int radius)
positions = distributor.getSphere(500).uniform(1000)
positions = distributor.getSphere(500).center(1000, { mu: 0, sigma: 0.2 })
positions = distributor.getSphere(500).edges(1000, { mu: 1, sigma: 0.1 })

//getSphere(int radius)
positions = distributor.getCircle(500).uniform(1000)
positions = distributor.getCircle(500).center(1000, { mu: 0.1, sigma: 0.3 })
positions = distributor.getCircle(500).edges(1000, { mu: 1, sigma: 0.2 })

```
