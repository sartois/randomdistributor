export declare function normal(mu: number, sigma: number): number;
export declare function uniform(min?: number, max?: number): number;
