import { ADistributor, type Position, type NormalDistributionConfig } from "./ADistributor";
export declare class CircleDistributor extends ADistributor {
    #private;
    constructor(radius: number);
    uniform(agentNb: number): Position[];
    /**
     * Config default: { mu: 0, sigma: 0.3 }
     */
    center(agentNb: number, config?: NormalDistributionConfig): Position[];
    /**
     * Config default: { mu: 1, sigma: 0.2 }
     */
    edges(agentNb: number, config?: NormalDistributionConfig): Position[];
    corners(agentNb: number, config?: NormalDistributionConfig): Position[];
}
