import { CircleDistributor } from "./CircleDistributor";
import { CubeDistributor } from "./CubeDistributor";
import { RectangleDistributor } from "./RectangleDistributor";
import { SphereDistributor } from "./SphereDistributor";
export declare const distributor: {
    getSphere: (radius: number) => SphereDistributor;
    getCircle: (radius: number) => CircleDistributor;
    getRectangle({ w, h }: {
        w: number;
        h: number;
    }): RectangleDistributor;
    getCube(w: number): CubeDistributor;
};
