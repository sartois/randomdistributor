import { ADistributor, type Position, type NormalDistributionConfig } from "./ADistributor";
export declare class CubeDistributor extends ADistributor {
    #private;
    constructor(halfSize: number);
    uniform(agentNb: number): Position[];
    /**
     * Config default values : { mu: 0.5, sigma: 0.1 }
     */
    center(agentNb: number, config?: NormalDistributionConfig): Position[];
    /**
     * Mu is not used here (randomly 0 or 1)
     * config default value : { sigma: 0.13 }
     */
    corners(agentNb: number, config?: NormalDistributionConfig): Position[];
    edges(agentNb: number, config?: NormalDistributionConfig): Position[];
}
