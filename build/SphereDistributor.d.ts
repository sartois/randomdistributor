import { ADistributor, type Position, type NormalDistributionConfig } from "./ADistributor";
export declare class SphereDistributor extends ADistributor {
    #private;
    constructor(radius: number);
    /**
     * Config default: { mu: 1, sigma: 0.1 }
     */
    edges(agentNb: number, config?: NormalDistributionConfig): Position[];
    corners(agentNb: number, config?: NormalDistributionConfig | undefined): Position[];
    uniform(agentNb: number): Position[];
    /**
     * Config default: { mu: 0, sigma: 0.2 }
     */
    center(agentNb: number, config?: NormalDistributionConfig): Position[];
}
