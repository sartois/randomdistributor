/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */


function __classPrivateFieldGet(receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
}

function __classPrivateFieldSet(receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

var defaultSource = Math.random;

var randomUniform = (function sourceRandomUniform(source) {
  function randomUniform(min, max) {
    min = min == null ? 0 : +min;
    max = max == null ? 1 : +max;
    if (arguments.length === 1) max = min, min = 0;
    else max -= min;
    return function() {
      return source() * max + min;
    };
  }

  randomUniform.source = sourceRandomUniform;

  return randomUniform;
})(defaultSource);

var randomNormal = (function sourceRandomNormal(source) {
  function randomNormal(mu, sigma) {
    var x, r;
    mu = mu == null ? 0 : +mu;
    sigma = sigma == null ? 1 : +sigma;
    return function() {
      var y;

      // If available, use the second previously-generated uniform random.
      if (x != null) y = x, x = null;

      // Otherwise, generate a new x and y.
      else do {
        x = source() * 2 - 1;
        y = source() * 2 - 1;
        r = x * x + y * y;
      } while (!r || r > 1);

      return mu + sigma * y * Math.sqrt(-2 * Math.log(r) / r);
    };
  }

  randomNormal.source = sourceRandomNormal;

  return randomNormal;
})(defaultSource);

function normal(mu, sigma) {
    return randomNormal(mu, sigma)();
}
function uniform(min = 0, max = 1) {
    return randomUniform(min, max)();
}

class ADistributor {
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

var _a$1, _CircleDistributor_twoPi, _CircleDistributor_radius;
class CircleDistributor extends ADistributor {
    constructor(radius) {
        super();
        _CircleDistributor_radius.set(this, void 0);
        __classPrivateFieldSet(this, _CircleDistributor_radius, radius, "f");
    }
    //See https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly#answer-50746409
    uniform(agentNb) {
        return this.loop(agentNb, () => {
            const r = __classPrivateFieldGet(this, _CircleDistributor_radius, "f") * Math.sqrt(Math.random());
            const theta = Math.random() * __classPrivateFieldGet(_a$1, _a$1, "f", _CircleDistributor_twoPi);
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    /**
     * Config default: { mu: 0, sigma: 0.3 }
     */
    center(agentNb, config) {
        const { mu, sigma } = config !== null && config !== void 0 ? config : { mu: 0, sigma: 0.3 };
        return this.loop(agentNb, () => {
            let h = -1;
            while (h < 0 || h > 1) {
                h = normal(mu, sigma);
            }
            const r = __classPrivateFieldGet(this, _CircleDistributor_radius, "f") * Math.pow(h, 2);
            const theta = Math.random() * __classPrivateFieldGet(_a$1, _a$1, "f", _CircleDistributor_twoPi);
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    /**
     * Config default: { mu: 1, sigma: 0.2 }
     */
    edges(agentNb, config) {
        if (config === undefined) {
            config = { mu: 1, sigma: 0.2 };
        }
        const { mu, sigma } = config;
        return this.loop(agentNb, () => {
            let h = -1;
            while (h < 0 || h >= 1) {
                h = normal(mu, sigma);
            }
            const r = __classPrivateFieldGet(this, _CircleDistributor_radius, "f") * Math.sqrt(h);
            const theta = Math.random() * __classPrivateFieldGet(_a$1, _a$1, "f", _CircleDistributor_twoPi);
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    corners(agentNb, config) {
        return this.edges(agentNb, config);
    }
}
_a$1 = CircleDistributor, _CircleDistributor_radius = new WeakMap();
_CircleDistributor_twoPi = { value: Math.PI * 2 };

var _CubeDistributor_size, _CubeDistributor_halfSize, _CubeDistributor_tuples;
class CubeDistributor extends ADistributor {
    constructor(halfSize) {
        super();
        _CubeDistributor_size.set(this, void 0);
        _CubeDistributor_halfSize.set(this, void 0);
        _CubeDistributor_tuples.set(this, [
            ["x", 0],
            ["y", 0],
            ["z", 0],
        ]);
        __classPrivateFieldSet(this, _CubeDistributor_size, halfSize * 2, "f");
        __classPrivateFieldSet(this, _CubeDistributor_halfSize, halfSize, "f");
    }
    uniform(agentNb) {
        return this.loop(agentNb, () => {
            return Object.fromEntries(__classPrivateFieldGet(this, _CubeDistributor_tuples, "f").map((tuple) => {
                tuple[1] = uniform(0, __classPrivateFieldGet(this, _CubeDistributor_size, "f")) - __classPrivateFieldGet(this, _CubeDistributor_halfSize, "f");
                return tuple;
            }));
        });
    }
    /**
     * Config default values : { mu: 0.5, sigma: 0.1 }
     */
    center(agentNb, config) {
        const { mu, sigma } = config !== null && config !== void 0 ? config : { mu: 0.5, sigma: 0.1 };
        return this.loop(agentNb, () => {
            return Object.fromEntries(__classPrivateFieldGet(this, _CubeDistributor_tuples, "f").map((tuple) => {
                tuple[1] = normal(mu, sigma) * __classPrivateFieldGet(this, _CubeDistributor_size, "f") - __classPrivateFieldGet(this, _CubeDistributor_halfSize, "f");
                return tuple;
            }));
        });
    }
    /**
     * Mu is not used here (randomly 0 or 1)
     * config default value : { sigma: 0.13 }
     */
    corners(agentNb, config) {
        const { sigma } = config !== null && config !== void 0 ? config : { sigma: 0.13 };
        return this.loop(agentNb, () => {
            return Object.fromEntries(__classPrivateFieldGet(this, _CubeDistributor_tuples, "f").map((tuple) => {
                let x = -1;
                while (!(x >= 0 && x < 1)) {
                    x = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
                }
                tuple[1] = x * __classPrivateFieldGet(this, _CubeDistributor_size, "f") - __classPrivateFieldGet(this, _CubeDistributor_halfSize, "f");
                return tuple;
            }));
        });
    }
    edges(agentNb, config) {
        return this.corners(agentNb, config);
    }
}
_CubeDistributor_size = new WeakMap(), _CubeDistributor_halfSize = new WeakMap(), _CubeDistributor_tuples = new WeakMap();

var _RectangleDistributor_width, _RectangleDistributor_height;
class RectangleDistributor extends ADistributor {
    constructor({ width, height }) {
        super();
        _RectangleDistributor_width.set(this, void 0);
        _RectangleDistributor_height.set(this, void 0);
        __classPrivateFieldSet(this, _RectangleDistributor_width, width, "f");
        __classPrivateFieldSet(this, _RectangleDistributor_height, height, "f");
    }
    uniform(agentNb) {
        return this.loop(agentNb, () => ({
            x: uniform(0, __classPrivateFieldGet(this, _RectangleDistributor_width, "f")),
            y: uniform(0, __classPrivateFieldGet(this, _RectangleDistributor_height, "f")),
            z: 0,
        }));
    }
    /**
     * Config default: { mu: 0.5, sigma: 0.1 }
     */
    center(agentNb, config) {
        const { mu, sigma } = config !== null && config !== void 0 ? config : { mu: 0.5, sigma: 0.1 };
        return this.loop(agentNb, () => ({
            x: normal(mu, sigma) * __classPrivateFieldGet(this, _RectangleDistributor_width, "f"),
            y: normal(mu, sigma) * __classPrivateFieldGet(this, _RectangleDistributor_height, "f"),
            z: 0,
        }));
    }
    /**
     * Mu is not used here (randomly 0 or 1)
     * Config default: { sigma: 0.13 }
     */
    corners(agentNb, config) {
        const { sigma } = config !== null && config !== void 0 ? config : { sigma: 0.13 };
        return this.loop(agentNb, () => {
            let x = -1, y = -1;
            while (!(x >= 0 && x < 1 && y >= 0 && y < 1)) {
                if (!(x > 0 && x < 1))
                    x = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
                if (!(y > 0 && y < 1))
                    y = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
            }
            return {
                x: x * __classPrivateFieldGet(this, _RectangleDistributor_width, "f"),
                y: y * __classPrivateFieldGet(this, _RectangleDistributor_height, "f"),
                z: 0,
            };
        });
    }
    edges(agentNb, config) {
        return this.corners(agentNb, config);
    }
}
_RectangleDistributor_width = new WeakMap(), _RectangleDistributor_height = new WeakMap();

function mapLinear(x, a1, a2, b1, b2) {
    return b1 + ((x - a1) * (b2 - b1)) / (a2 - a1);
}
function scalarMultiplication(p, scalar) {
    p.x *= scalar;
    p.y *= scalar;
    p.z *= scalar;
    return p;
}
//Derived from : https://mathworld.wolfram.com/SpherePointPicking.html
//Adapted from : https://github.com/mrdoob/three.js/blob/dev/src/math/Vector3.js#L695
function randomDirection() {
    const u = (Math.random() - 0.5) * 2;
    const t = Math.random() * Math.PI * 2;
    const f = Math.sqrt(1 - u ** 2);
    return {
        x: f * Math.cos(t),
        y: f * Math.sin(t),
        z: u,
    };
}

var _a, _SphereDistributor_twoPI, _SphereDistributor_radius;
//For the math see https://karthikkaranth.me/blog/generating-random-points-in-a-sphere/
class SphereDistributor extends ADistributor {
    constructor(radius) {
        super();
        _SphereDistributor_radius.set(this, void 0);
        __classPrivateFieldSet(this, _SphereDistributor_radius, radius, "f");
    }
    /**
     * Config default: { mu: 1, sigma: 0.1 }
     */
    edges(agentNb, config) {
        const { mu, sigma } = config !== null && config !== void 0 ? config : { mu: 1, sigma: 0.1 };
        return this.center(agentNb, { mu, sigma });
    }
    corners(agentNb, config) {
        return this.edges(agentNb, config);
    }
    //@todo is this really uniform ?
    uniform(agentNb) {
        return this.loop(agentNb, () => {
            const u = Math.random(), v = Math.random();
            const theta = u * __classPrivateFieldGet(_a, _a, "f", _SphereDistributor_twoPI);
            const phi = Math.acos(2.0 * v - 1.0);
            const r = Math.cbrt(Math.random());
            const sinTheta = Math.sin(theta);
            const cosTheta = Math.cos(theta);
            const sinPhi = Math.sin(phi);
            const cosPhi = Math.cos(phi);
            return {
                x: mapLinear(r * sinPhi * cosTheta, -1, 1, -__classPrivateFieldGet(this, _SphereDistributor_radius, "f"), __classPrivateFieldGet(this, _SphereDistributor_radius, "f")),
                y: mapLinear(r * sinPhi * sinTheta, -1, 1, -__classPrivateFieldGet(this, _SphereDistributor_radius, "f"), __classPrivateFieldGet(this, _SphereDistributor_radius, "f")),
                z: mapLinear(r * cosPhi, -1, 1, -__classPrivateFieldGet(this, _SphereDistributor_radius, "f"), __classPrivateFieldGet(this, _SphereDistributor_radius, "f")),
            };
        });
    }
    /**
     * Config default: { mu: 0, sigma: 0.2 }
     */
    center(agentNb, config) {
        const { mu, sigma } = config !== null && config !== void 0 ? config : { mu: 0, sigma: 0.2 };
        return this.loop(agentNb, () => {
            let r = -1;
            while (r < 0 || r > 1) {
                r = normal(mu, sigma);
            }
            return scalarMultiplication(randomDirection(), r * __classPrivateFieldGet(this, _SphereDistributor_radius, "f"));
        });
    }
}
_a = SphereDistributor, _SphereDistributor_radius = new WeakMap();
_SphereDistributor_twoPI = { value: Math.PI * 2 };

const distributor = {
    getSphere: (radius) => {
        return new SphereDistributor(radius);
    },
    getCircle: (radius) => {
        return new CircleDistributor(radius);
    },
    getRectangle({ w, h }) {
        return new RectangleDistributor({ width: w, height: h });
    },
    getCube(w) {
        return new CubeDistributor(w);
    },
};

export { distributor };
