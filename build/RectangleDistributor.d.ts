import { ADistributor, type NormalDistributionConfig } from "./ADistributor";
export interface Rectangle {
    width: number;
    height: number;
}
export declare class RectangleDistributor extends ADistributor {
    #private;
    constructor({ width, height }: Rectangle);
    uniform(agentNb: number): import("./ADistributor").Position[];
    /**
     * Config default: { mu: 0.5, sigma: 0.1 }
     */
    center(agentNb: number, config?: NormalDistributionConfig): import("./ADistributor").Position[];
    /**
     * Mu is not used here (randomly 0 or 1)
     * Config default: { sigma: 0.13 }
     */
    corners(agentNb: number, config?: NormalDistributionConfig): import("./ADistributor").Position[];
    edges(agentNb: number, config?: NormalDistributionConfig): import("./ADistributor").Position[];
}
