export interface Position {
    x: number;
    y: number;
    z: number;
}
export interface NormalDistributionConfig {
    mu?: number;
    sigma: number;
}
export type PositionTuple = [keyof Position, number];
export declare abstract class ADistributor {
    abstract uniform(agentNb: number): Position[];
    abstract center(agentNb: number, config?: NormalDistributionConfig): Position[];
    abstract edges(agentNb: number, config?: NormalDistributionConfig): Position[];
    abstract corners(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: () => Position): Position[];
}
