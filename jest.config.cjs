/** @type {import('ts-jest').JestConfigWithTsJest} */
/* eslint-disable */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  moduleNameMapper: {
    "^d3-random$": "<rootDir>/node_modules/d3-random/dist/d3-random.min.js",
  },
};
