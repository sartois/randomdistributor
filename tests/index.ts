//import { type Position } from "../src/ADistributor";
//import { Vector3 } from "three";

export function ensureResultsAreValidPosition(results: any[]) {
  results.forEach((position) => {
    ["x", "y", "z"].forEach((prop) => {
      expect(position).toHaveProperty(prop);
    });
  });
}

export function ensureResultsAreInTheExpectedInterval(
  results: any[],
  interval: [number, number],
) {
  results.forEach((position) => {
    Object.entries(position).forEach(([, value]) => {
      expect(value).toBeGreaterThanOrEqual(interval[0]);
      expect(value).toBeLessThanOrEqual(interval[1]);
    });
  });
}

/* export function getMathInfo(results: Position[]) {
  const centroid = new Vector3()
  results.forEach(p => {centroid.add(p as Vector3)})
  centroid.divideScalar(results.length)

  const standardsDeviation = [0,0,0]
  results.reduce((acc, cur) => {

  }, standardsDeviation)
} */
