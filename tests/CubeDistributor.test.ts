import {
  ensureResultsAreInTheExpectedInterval,
  ensureResultsAreValidPosition,
} from "./index";
import { CubeDistributor } from "../src/CubeDistributor";

describe("CubeDistributor", () => {
  let cubeDistributor: CubeDistributor;
  it("center", () => {
    cubeDistributor = new CubeDistributor(1);
    const results = cubeDistributor.center(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-1, 1]);
  });
  it("uniform", () => {
    cubeDistributor = new CubeDistributor(1);
    const results = cubeDistributor.uniform(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-1, 1]);
  });
  it("corners", () => {
    cubeDistributor = new CubeDistributor(1);
    const results = cubeDistributor.corners(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-1, 1]);
  });
});
