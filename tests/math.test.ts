import { normal, uniform } from "../src/math";
import { randomNormal, randomUniform } from "d3-random";

jest.mock("d3-random", () => {
  return {
    randomNormal: jest.fn(() => {
      return () => 1;
    }),
    randomUniform: jest.fn(() => {
      return () => 1;
    }),
  };
});

describe("math", () => {
  it("normal must call d3-random.randomNormal with passed mu and sigma", () => {
    normal(1, 2);

    expect(randomNormal).toHaveBeenCalledTimes(1);
    //@ts-ignore
    expect(randomNormal.mock.calls[0][0]).toBe(1);
    //@ts-ignore
    expect(randomNormal.mock.calls[0][1]).toBe(2);
  });

  it("uniform must call d3-random.randomUniform with passed min and max", () => {
    uniform(0, 1);

    expect(randomUniform).toHaveBeenCalledTimes(1);
    //@ts-ignore
    expect(randomUniform.mock.calls[0][0]).toBe(0);
    //@ts-ignore
    expect(randomUniform.mock.calls[0][1]).toBe(1);
  });
});
