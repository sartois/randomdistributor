import {
  ensureResultsAreInTheExpectedInterval,
  ensureResultsAreValidPosition,
} from "./index";
import { SphereDistributor } from "../src/SphereDistributor";

describe("SphereDistributor", () => {
  let sphereDistributor: SphereDistributor;

  beforeEach(() => {
    sphereDistributor = new SphereDistributor(0.5);
  });

  it("center", () => {
    const results = sphereDistributor.center(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
  it("uniform", () => {
    const results = sphereDistributor.uniform(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
  it("edges", () => {
    const results = sphereDistributor.edges(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
});
