import { CircleDistributor } from "../src/CircleDistributor";
import { CubeDistributor } from "../src/CubeDistributor";
import { RectangleDistributor } from "../src/RectangleDistributor";
import { SphereDistributor } from "../src/SphereDistributor";
import { distributor } from "../src/distributor";

describe("distributor", () => {
  it("getSphere must return a SphereDistributor", () => {
    expect(distributor.getSphere(1)).toBeInstanceOf(SphereDistributor);
  });
  it("getCircle must return a CircleDistributor", () => {
    expect(distributor.getCircle(1)).toBeInstanceOf(CircleDistributor);
  });
  it("getRectangle must return a RectangleDistributor", () => {
    expect(distributor.getRectangle({ w: 1, h: 1 })).toBeInstanceOf(
      RectangleDistributor,
    );
  });
  it("getCube must return a CubeDistributor", () => {
    expect(distributor.getCube(1)).toBeInstanceOf(CubeDistributor);
  });
});
