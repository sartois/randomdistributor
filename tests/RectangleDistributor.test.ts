import {
  ensureResultsAreInTheExpectedInterval,
  ensureResultsAreValidPosition,
} from "./index";
import { type Position } from "../src/ADistributor";
import { RectangleDistributor } from "../src/RectangleDistributor";

describe("RectangleDistributor", () => {
  let rectangleDistributor: RectangleDistributor;

  beforeEach(() => {
    rectangleDistributor = new RectangleDistributor({ width: 1, height: 1 });
  });

  it("center results should have z set to 0 and x/y between width/height", () => {
    const results = rectangleDistributor.center(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [0, 1]);
    ensureResultsZisZero(results);
  });
  it("uniform results should have z set to 0 and x/y between width/height", () => {
    const results = rectangleDistributor.uniform(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [0, 1]);
    ensureResultsZisZero(results);
  });
  it("corners results should have z set to 0 and x/y between width/height", () => {
    const results = rectangleDistributor.uniform(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [0, 1]);
    ensureResultsZisZero(results);
  });
});

function ensureResultsZisZero(results: Position[]) {
  results.forEach((p) => {
    expect(p.z).toBe(0);
  });
}
