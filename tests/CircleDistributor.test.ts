import {
  ensureResultsAreInTheExpectedInterval,
  ensureResultsAreValidPosition,
} from "./index";
import { CircleDistributor } from "../src/CircleDistributor";

describe("CircleDistributor", () => {
  let circleDistributor: CircleDistributor;
  it("center", () => {
    circleDistributor = new CircleDistributor(0.5);
    const results = circleDistributor.center(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
  it("uniform", () => {
    circleDistributor = new CircleDistributor(0.5);
    const results = circleDistributor.uniform(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
  it("edges", () => {
    circleDistributor = new CircleDistributor(0.5);
    const results = circleDistributor.edges(10);
    expect(results.length).toBe(10);
    ensureResultsAreValidPosition(results);
    ensureResultsAreInTheExpectedInterval(results, [-0.5, 0.5]);
  });
});
