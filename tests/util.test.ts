import { type Position } from "../src/ADistributor";
import { mapLinear, randomDirection, scalarMultiplication } from "../src/util";

describe("util", () => {
  describe("mapLinear", () => {
    it("should return expected value for simple case", () => {
      const result = mapLinear(0.5, 0, 1, 2, 4);
      expect(result).toBe(3);
    });

    it("should return expected value even with negative range", () => {
      const result = mapLinear(0, -1, 1, 2, 4);
      expect(result).toBe(3);
    });

    it("should return lower boundary if number is equal to the lower limit", () => {
      const result = mapLinear(0, 0, 1, 2, 4);
      expect(result).toBe(2);
    });

    it("should return higher boundary if number is equal to the upper limit", () => {
      const result = mapLinear(1, 0, 1, 2, 4);
      expect(result).toBe(4);
    });
  });
  describe("randomDirection", () => {
    it("should return coherent value", () => {
      const result = randomDirection();
      expect(length(result)).toBeCloseTo(1, 5);
      expect(result.x).toBeLessThanOrEqual(1);
      expect(result.y).toBeLessThanOrEqual(1);
      expect(result.z).toBeLessThanOrEqual(1);
    });
  });
  describe("scalarMultiplication", () => {
    it("should return coherent value", () => {
      const p = { x: 1, y: 1, z: 1 };
      scalarMultiplication(p, 2);
      expect(p.x).toBeLessThanOrEqual(2);
      expect(p.y).toBeLessThanOrEqual(2);
      expect(p.z).toBeLessThanOrEqual(2);
    });
  });
});

function length(p: Position) {
  return Math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
}
