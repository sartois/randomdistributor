import { ADistributor, type NormalDistributionConfig } from "./ADistributor";
import { normal, uniform } from "./math";

export interface Rectangle {
  width: number;
  height: number;
}

export class RectangleDistributor extends ADistributor {
  #width: number;
  #height: number;

  constructor({ width, height }: Rectangle) {
    super();
    this.#width = width;
    this.#height = height;
  }

  uniform(agentNb: number) {
    return this.loop(agentNb, () => ({
      x: uniform(0, this.#width),
      y: uniform(0, this.#height),
      z: 0,
    }));
  }

  /**
   * Config default: { mu: 0.5, sigma: 0.1 }
   */
  center(agentNb: number, config?: NormalDistributionConfig) {
    const { mu, sigma } = config ?? { mu: 0.5, sigma: 0.1 };
    return this.loop(agentNb, () => ({
      x: normal(mu!, sigma) * this.#width,
      y: normal(mu!, sigma) * this.#height,
      z: 0,
    }));
  }

  /**
   * Mu is not used here (randomly 0 or 1)
   * Config default: { sigma: 0.13 }
   */
  corners(agentNb: number, config?: NormalDistributionConfig) {
    const { sigma } = config ?? { sigma: 0.13 };
    return this.loop(agentNb, () => {
      let x = -1,
        y = -1;

      while (!(x >= 0 && x < 1 && y >= 0 && y < 1)) {
        if (!(x > 0 && x < 1))
          x = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
        if (!(y > 0 && y < 1))
          y = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
      }

      return {
        x: x * this.#width,
        y: y * this.#height,
        z: 0,
      };
    });
  }

  edges(agentNb: number, config?: NormalDistributionConfig) {
    return this.corners(agentNb, config);
  }
}
