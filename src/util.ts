import { Position } from "./ADistributor";

export function mapLinear(
  x: number,
  a1: number,
  a2: number,
  b1: number,
  b2: number,
) {
  return b1 + ((x - a1) * (b2 - b1)) / (a2 - a1);
}

export function scalarMultiplication(p: Position, scalar: number) {
  p.x *= scalar;
  p.y *= scalar;
  p.z *= scalar;

  return p;
}

//Derived from : https://mathworld.wolfram.com/SpherePointPicking.html
//Adapted from : https://github.com/mrdoob/three.js/blob/dev/src/math/Vector3.js#L695
export function randomDirection() {
  const u = (Math.random() - 0.5) * 2;
  const t = Math.random() * Math.PI * 2;
  const f = Math.sqrt(1 - u ** 2);

  return {
    x: f * Math.cos(t),
    y: f * Math.sin(t),
    z: u,
  };
}
