import {
  ADistributor,
  type Position,
  type NormalDistributionConfig,
  type PositionTuple,
} from "./ADistributor";
import { normal, uniform } from "./math";

export class CubeDistributor extends ADistributor {
  #size: number;
  #halfSize: number;
  #tuples: PositionTuple[] = [
    ["x", 0],
    ["y", 0],
    ["z", 0],
  ];

  constructor(halfSize: number) {
    super();
    this.#size = halfSize * 2;
    this.#halfSize = halfSize;
  }

  uniform(agentNb: number) {
    return this.loop(agentNb, () => {
      return Object.fromEntries(
        this.#tuples.map((tuple) => {
          tuple[1] = uniform(0, this.#size) - this.#halfSize;
          return tuple;
        }),
      ) as unknown as Position;
    });
  }

  /**
   * Config default values : { mu: 0.5, sigma: 0.1 }
   */
  center(agentNb: number, config?: NormalDistributionConfig) {
    const { mu, sigma } = config ?? { mu: 0.5, sigma: 0.1 };
    return this.loop(agentNb, () => {
      return Object.fromEntries(
        this.#tuples.map((tuple) => {
          tuple[1] = normal(mu!, sigma) * this.#size - this.#halfSize;
          return tuple;
        }),
      ) as unknown as Position;
    });
  }

  /**
   * Mu is not used here (randomly 0 or 1)
   * config default value : { sigma: 0.13 }
   */
  corners(agentNb: number, config?: NormalDistributionConfig) {
    const { sigma } = config ?? { sigma: 0.13 };
    return this.loop(agentNb, () => {
      return Object.fromEntries(
        this.#tuples.map((tuple) => {
          let x = -1;
          while (!(x >= 0 && x < 1)) {
            x = Math.random() > 0.5 ? normal(0, sigma) : normal(1, sigma);
          }
          tuple[1] = x * this.#size - this.#halfSize;
          return tuple;
        }),
      ) as unknown as Position;
    });
  }

  edges(agentNb: number, config?: NormalDistributionConfig) {
    return this.corners(agentNb, config);
  }
}
