import { SphereDistributorViz } from "./components/SphereDistributorViz";
import { Meta, StoryObj } from "@storybook/react";

const meta = {
  title: "distibutor/Sphere",
  component: SphereDistributorViz,
  parameters: {
    debug: true,
    backgroundColor: "#F2F2F2",
  },
} satisfies Meta<typeof SphereDistributorViz>;

export default meta;
type Story = StoryObj<typeof meta>;

export const SphereDistribution: Story = {
  args: {
    type: "uniform",
    agentCount: 1000,
    radius: 10,
    centerDistributionConfig: { mu: 0, sigma: 0.2 },
    edgesDistributionConfig: { mu: 1, sigma: 0.13 },
  },
  argTypes: {
    type: {
      options: ["uniform", "center", "edges"],
      control: {
        type: "radio",
      },
    },
  },
};
