import { CircleDistributorViz } from "./components/CircleDistributorViz";
import { Meta, StoryObj } from "@storybook/react";

const meta = {
  title: "distibutor/Circle",
  component: CircleDistributorViz,
  parameters: {
    debug: true,
    backgroundColor: "#F2F2F2",
  },
} satisfies Meta<typeof CircleDistributorViz>;

export default meta;
type Story = StoryObj<typeof meta>;

export const CircleDistribution: Story = {
  args: {
    type: "uniform",
    agentCount: 1000,
    radius: 10,
    centerDistributionConfig: {
      mu: 0,
      sigma: 0.3,
    },
    edgesDistributionConfig: {
      mu: 1,
      sigma: 0.2,
    },
  },
  argTypes: {
    type: {
      options: ["uniform", "center", "edges"],
      control: {
        type: "radio",
      },
    },
  },
};
