import { CubeDistributorViz } from "./components/CubeDistributorViz";
import { Meta, StoryObj } from "@storybook/react";

const meta = {
  title: "distibutor/Cube",
  component: CubeDistributorViz,
  parameters: {
    debug: true,
    backgroundColor: "#F2F2F2",
  },
} satisfies Meta<typeof CubeDistributorViz>;

export default meta;
type Story = StoryObj<typeof meta>;

export const CubeDistribution: Story = {
  args: {
    type: "uniform",
    agentCount: 1000,
    width: 10,
    centerDistributionConfig: { mu: 0.5, sigma: 0.1 },
    edgesDistributionConfig: { sigma: 0.13 },
  },
  argTypes: {
    type: {
      options: ["uniform", "center", "corners"],
      control: {
        type: "radio",
      },
    },
  },
};
