import { RectangleDistributorViz } from "./components/RectangleDistributorViz";
import { Meta, StoryObj } from "@storybook/react";

const meta = {
  title: "distibutor/Rectangle",
  component: RectangleDistributorViz,
  parameters: {
    debug: true,
    backgroundColor: "#F2F2F2",
  },
} satisfies Meta<typeof RectangleDistributorViz>;

export default meta;
type Story = StoryObj<typeof meta>;

export const RectangleDistribution: Story = {
  args: {
    type: "uniform",
    agentCount: 1000,
    width: 10,
    height: 10,
    centerDistributionConfig: { mu: 0.5, sigma: 0.1 },
    edgesDistributionConfig: { sigma: 0.13 },
  },
  argTypes: {
    type: {
      options: ["uniform", "center", "corners"],
      control: {
        type: "radio",
      },
    },
  },
};
