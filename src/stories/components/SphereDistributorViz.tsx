import React from "react";
import { NormalDistributionConfig } from "../../ADistributor";
import { distributor } from "../../distributor";
import { Agent } from "../helpers/Agent";

interface SphereDistributorVizProps {
  type: "uniform" | "center" | "edges";
  agentCount: number;
  radius: number;
  centerDistributionConfig: NormalDistributionConfig;
  edgesDistributionConfig: NormalDistributionConfig;
}

export function SphereDistributorViz({
  type,
  agentCount,
  radius,
  centerDistributionConfig,
  edgesDistributionConfig,
}: SphereDistributorVizProps) {
  const circleDistributor = distributor.getSphere(radius);
  const optParam =
    type === "center"
      ? centerDistributionConfig
      : type === "edges"
        ? edgesDistributionConfig
        : undefined;
  const positions = circleDistributor[type](agentCount, optParam);

  return (
    <>
      {positions.map((p, i) => (
        <Agent position={p} key={`agent-${i}`}>
          <mesh>
            <boxGeometry args={[0.5, 0.5, 0.5]} />
            <meshStandardMaterial color={"#D97904"} />
          </mesh>
        </Agent>
      ))}
      <mesh>
        <dodecahedronGeometry args={[radius, 3]} />
        <meshBasicMaterial wireframe={true} />
      </mesh>
    </>
  );
}
