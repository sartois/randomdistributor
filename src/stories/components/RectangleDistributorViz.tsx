import React from "react";
import { NormalDistributionConfig } from "../../ADistributor";
import { distributor } from "../../distributor";
import { Agent } from "../helpers/Agent";

interface RectangleDistributorVizProps {
  type: "uniform" | "center" | "corners";
  agentCount: number;
  width: number;
  height: number;
  centerDistributionConfig: NormalDistributionConfig;
  edgesDistributionConfig: NormalDistributionConfig;
}

export function RectangleDistributorViz({
  type,
  agentCount,
  width,
  height,
  centerDistributionConfig,
  edgesDistributionConfig,
}: RectangleDistributorVizProps) {
  const circleDistributor = distributor.getRectangle({ w: width, h: height });
  const optParam =
    type === "center"
      ? centerDistributionConfig
      : type === "corners"
        ? edgesDistributionConfig
        : undefined;
  const positions = circleDistributor[type](agentCount, optParam).map((p) => ({
    x: p.x,
    y: p.z,
    z: p.y,
  }));

  return (
    <>
      {positions.map((p, i) => (
        <Agent position={p} key={`agent-${i}`}>
          <mesh>
            <boxGeometry args={[0.5, 0.5, 0.5]} />
            <meshStandardMaterial color={"#D97904"} />
          </mesh>
        </Agent>
      ))}
      <mesh
        rotation-x={Math.PI * 0.5}
        position={[width * 0.5, 0, height * 0.5]}
      >
        <planeGeometry args={[width, height]} />
        <meshStandardMaterial wireframe={true} />
      </mesh>
    </>
  );
}
