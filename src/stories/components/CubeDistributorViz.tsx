import React from "react";
import { NormalDistributionConfig } from "../../ADistributor";
import { distributor } from "../../distributor";
import { Agent } from "../helpers/Agent";

interface CubeDistributorVizProps {
  type: "uniform" | "center" | "corners";
  agentCount: number;
  width: number;
  centerDistributionConfig: NormalDistributionConfig;
  edgesDistributionConfig: NormalDistributionConfig;
}

export function CubeDistributorViz({
  type,
  agentCount,
  width,
  centerDistributionConfig,
  edgesDistributionConfig,
}: CubeDistributorVizProps) {
  const circleDistributor = distributor.getCube(width);
  const optParam =
    type === "center"
      ? centerDistributionConfig
      : type === "corners"
        ? edgesDistributionConfig
        : undefined;
  const positions = circleDistributor[type](agentCount, optParam);

  return (
    <>
      {positions.map((p, i) => (
        <Agent position={p} key={`agent-${i}`}>
          <mesh>
            <boxGeometry args={[0.5, 0.5, 0.5]} />
            <meshStandardMaterial color={"#D97904"} />
          </mesh>
        </Agent>
      ))}
      <mesh>
        <boxGeometry args={[width * 2, width * 2, width * 2]} />
        <meshStandardMaterial wireframe={true} />
      </mesh>
    </>
  );
}
