import React from "react";
import { NormalDistributionConfig } from "../../ADistributor";
import { distributor } from "../../distributor";
import { Agent } from "../helpers/Agent";

interface CircleDistributorVizProps {
  type: "uniform" | "center" | "edges";
  agentCount: number;
  radius: number;
  centerDistributionConfig: NormalDistributionConfig;
  edgesDistributionConfig: NormalDistributionConfig;
}

export function CircleDistributorViz({
  type,
  agentCount,
  radius,
  centerDistributionConfig,
  edgesDistributionConfig,
}: CircleDistributorVizProps) {
  const circleDistributor = distributor.getCircle(radius);
  const optParam =
    type === "center"
      ? centerDistributionConfig
      : type === "edges"
        ? edgesDistributionConfig
        : undefined;
  const positions = circleDistributor[type](agentCount, optParam).map((p) => ({
    x: p.x,
    y: p.z,
    z: p.y,
  }));

  return (
    <>
      {positions.map((p, i) => (
        <Agent position={p} key={`agent-${i}`}>
          <mesh>
            <boxGeometry args={[0.5, 0.5, 0.5]} />
            <meshStandardMaterial color={"#D97904"} />
          </mesh>
        </Agent>
      ))}
      <mesh rotation-x={Math.PI * 0.5}>
        <circleGeometry args={[radius]} />
        <meshStandardMaterial wireframe={true} />
      </mesh>
    </>
  );
}
