import React, { useRef } from "react";
import { PropsWithChildren } from "react";
import { Group, Object3D, Vector3 } from "three";
import { type Position } from "../../../src/ADistributor";
import { useFrame } from "@react-three/fiber";

type AgentProps = {
  position: Position;
};

export function Agent(props: PropsWithChildren<AgentProps>) {
  const { children, position } = props;
  const ref = useRef<Group>(null!);

  useFrame(() => {
    if (ref?.current) {
      followPosition(position, ref.current);
    }
  });

  return (
    <group position={position as Vector3} ref={ref}>
      {children}
    </group>
  );
}

function followPosition(position: Position, mesh: Object3D) {
  mesh.position.set(position.x, position.y, position.z);
}
