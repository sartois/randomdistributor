import { mapLinear, randomDirection, scalarMultiplication } from "./util";
import {
  ADistributor,
  type Position,
  type NormalDistributionConfig,
} from "./ADistributor";
import { normal } from "./math";

//For the math see https://karthikkaranth.me/blog/generating-random-points-in-a-sphere/
export class SphereDistributor extends ADistributor {
  static #twoPI = Math.PI * 2;
  #radius: number;

  constructor(radius: number) {
    super();
    this.#radius = radius;
  }

  /**
   * Config default: { mu: 1, sigma: 0.1 }
   */
  edges(agentNb: number, config?: NormalDistributionConfig) {
    const { mu, sigma } = config ?? { mu: 1, sigma: 0.1 };
    return this.center(agentNb, { mu, sigma });
  }

  corners(
    agentNb: number,
    config?: NormalDistributionConfig | undefined,
  ): Position[] {
    return this.edges(agentNb, config);
  }

  //@todo is this really uniform ?
  uniform(agentNb: number) {
    return this.loop(agentNb, () => {
      const u = Math.random(),
        v = Math.random();
      const theta = u * SphereDistributor.#twoPI;
      const phi = Math.acos(2.0 * v - 1.0);
      const r = Math.cbrt(Math.random());
      const sinTheta = Math.sin(theta);
      const cosTheta = Math.cos(theta);
      const sinPhi = Math.sin(phi);
      const cosPhi = Math.cos(phi);

      return {
        x: mapLinear(r * sinPhi * cosTheta, -1, 1, -this.#radius, this.#radius),
        y: mapLinear(r * sinPhi * sinTheta, -1, 1, -this.#radius, this.#radius),
        z: mapLinear(r * cosPhi, -1, 1, -this.#radius, this.#radius),
      };
    });
  }

  /**
   * Config default: { mu: 0, sigma: 0.2 }
   */
  center(agentNb: number, config?: NormalDistributionConfig) {
    const { mu, sigma } = config ?? { mu: 0, sigma: 0.2 };
    return this.loop(agentNb, () => {
      let r = -1;
      while (r < 0 || r > 1) {
        r = normal(mu!, sigma);
      }

      return scalarMultiplication(randomDirection(), r * this.#radius);
    });
  }
}
