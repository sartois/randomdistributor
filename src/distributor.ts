import { CircleDistributor } from "./CircleDistributor";
import { CubeDistributor } from "./CubeDistributor";
import { RectangleDistributor } from "./RectangleDistributor";
import { SphereDistributor } from "./SphereDistributor";

export const distributor = {
  getSphere: (radius: number) => {
    return new SphereDistributor(radius);
  },
  getCircle: (radius: number) => {
    return new CircleDistributor(radius);
  },
  getRectangle({ w, h }: { w: number; h: number }) {
    return new RectangleDistributor({ width: w, height: h });
  },
  getCube(w: number) {
    return new CubeDistributor(w);
  },
};
