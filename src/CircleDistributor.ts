import { normal } from "./math";
import {
  ADistributor,
  type Position,
  type NormalDistributionConfig,
} from "./ADistributor";

export class CircleDistributor extends ADistributor {
  static #twoPi = Math.PI * 2;
  #radius: number;

  constructor(radius: number) {
    super();
    this.#radius = radius;
  }

  //See https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly#answer-50746409
  uniform(agentNb: number) {
    return this.loop(agentNb, () => {
      const r = this.#radius * Math.sqrt(Math.random());
      const theta = Math.random() * CircleDistributor.#twoPi;
      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
    });
  }

  /**
   * Config default: { mu: 0, sigma: 0.3 }
   */
  center(agentNb: number, config?: NormalDistributionConfig) {
    const { mu, sigma } = config ?? { mu: 0, sigma: 0.3 };

    return this.loop(agentNb, () => {
      let h = -1;
      while (h < 0 || h > 1) {
        h = normal(mu!, sigma);
      }

      const r = this.#radius * Math.pow(h, 2);
      const theta = Math.random() * CircleDistributor.#twoPi;

      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
    });
  }

  /**
   * Config default: { mu: 1, sigma: 0.2 }
   */
  edges(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 1, sigma: 0.2 };
    }

    const { mu, sigma } = config;

    return this.loop(agentNb, () => {
      let h = -1;
      while (h < 0 || h >= 1) {
        h = normal(mu!, sigma);
      }

      const r = this.#radius * Math.sqrt(h);
      const theta = Math.random() * CircleDistributor.#twoPi;

      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
    });
  }

  corners(agentNb: number, config?: NormalDistributionConfig): Position[] {
    return this.edges(agentNb, config);
  }
}
