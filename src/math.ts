import { randomNormal, randomUniform } from "d3-random";

export function normal(mu: number, sigma: number): number {
  return randomNormal(mu, sigma)();
}

export function uniform(min = 0, max = 1) {
  return randomUniform(min, max)();
}
